<?php require_once './code.php';?>

<!DOCTYPE html>
<html>
<head>
    <title>Index Page</title>
</head>
<body>
    <h2>Building</h2>
    <p>The name of the building is <?php echo $building->getBuildingName(); ?></p>
    <p>The <?php echo $building->getBuildingName(); ?> has <?php echo $building->getFloors(); ?> floors</p>
    <p>The <?php echo $building->getBuildingName(); ?> is located at <?php echo $building->getAddress(); ?></p>
    <?php $building->setBuildingName("Caswynn Complex"); ?>
    <p>The name of the building has been changed to <?php echo $building->getBuildingName(); ?></p>

    <h2>Condominium</h2>
    <p>The name of the condominium is <?php echo $condo->getBuildingName(); ?></p>
    <p>The <?php echo $condo->getBuildingName(); ?> has <?php echo $condo->getFloors(); ?> floors</p>
    <p>The <?php echo $condo->getBuildingName(); ?> is located at <?php echo $condo->getAddress(); ?></p>
    <?php $condo->setBuildingName("Enzo Tower"); ?>
    <p>The name of the condominium has been changed to <?php echo $condo->getBuildingName(); ?></p>
</body>
</html>

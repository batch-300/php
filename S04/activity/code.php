<?php
class Building {
    protected $floors;
    protected $address;
    protected $buildingName;

    public function __construct($floors, $address, $buildingName) {
        $this->floors = $floors;
        $this->address = $address;
        $this->buildingName = $buildingName;
    }

    public function getFloors() {
        return $this->getPrivateFloors();
    }

    public function getAddress() {
        return $this->getPrivateAddress();
    }

    private function getPrivateFloors() {
        return $this->floors;
    }

    private function getPrivateAddress() {
        return $this->address;
    }

    public function getBuildingName() {
        return $this->buildingName;
    }

    public function setBuildingName($buildingName) {
        $this->buildingName = $buildingName;
    }
}

class Condominium extends Building {

}

$building = new Building(8, "Timog Avenue, Quezon City, Philippines", "Caswynn Building");
$condo = new Condominium(5, "Buendia Avenue, Makati City, Philippines", "Enzo Condo");

<?php

class Building {

//Access modifiers
		//These are keywords that can bse used to control the visiblity of the properties and methods in a class

		//1. Public: fully open, properties or methods can be accessed from everywhere
		//2. Private: properties or methods can only be accessed within the class and disables inheritance
		//3. protected: Properties or methods is only accessible within the class and on its child class.


    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
}

class Condominium extends Building{

	//These Functions serves as the intermiary in accessing the object's property
	//These functions therefore defines whether an object's property can be accessed or changed
	//Getters and Setters
		//These are used to implement the encapsulation of an objects data
		//These are used to retrieve and modify values of each property of the object
		//Each property of an object should have a set of getters and setters.

	//Encapsulation - indicates that data must not be directly accesible to user but can be accesed only through public functions (setter and getter functions)

	//Getters and Setters

	//Getter - accessors
		//This is used to retrieve/access the value of an insantiated object

	public function getName(){
		return $this->name;
	}

	//Setter - mutators
		//This is used to change or modify the default value of a property of an instantiated object
	//setter functions can also be modified to add data validations
	public function setName($name){

		if(gettype($name) === "string"){
			$this->name =$name;
		}
	}
}

$building = new Building('Caswyn Building', 8, 'Timod Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo condo',5,'Buendia Avenue, Makati City, Philippines');

// Mini Activity

// Create a player class with these properties and methods
//Properties
	//username
	//email
	//password
	//level
	//guild
//methods
	//attack
	//defend

//Create a Mage class that is derived from the player class
//Create a getter and setter to change and retrieve the username and guild of the mage

//instantiate three mage objects/instances

class Player {
    protected $username;
    protected $email;
    protected $password;
    protected $level;
    protected $guild;
    protected $health = 100;

    public function __construct($username, $email, $password, $level, $guild) {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->level = $level;
        $this->guild = $guild;
    }

    public function attack() {
        echo $this->username . " attacks for " . $this->level * 10 . " damage!";
        return $this->level * 10;
    }

    public function defend($damage) {
        $this->health -= $damage;
        echo $this->username . " defends and loses " . $damage . " health. Current health: " . $this->health;
    }

    public function getLevel() {
        return $this->level;
    }

    public function getHealth() {
        return $this->health;
    }

 }

class Mage extends Player {
    public function __construct($username, $email, $password, $level, $guild) {
        parent::__construct($username, $email, $password, $level, $guild);
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getGuild() {
        return $this->guild;
    }

    public function setGuild($guild) {
        $this->guild = $guild;
    }
}

$mage1 = new Mage('Mage1', 'mage1@example.com', 'password1', 1, 'Guild1');
$mage2 = new Mage('Mage2', 'mage2@example.com', 'password2', 2, 'Guild2');
$mage3 = new Mage('Mage3', 'mage3@example.com', 'password3', 3, 'Guild3');
<?php require_once './code.php'?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S04: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Access modifiers</h1>
	<h2>Building variables</h2>
<!-- 	<p><?php //echo $building->name;?></p> -->
	<h2>Condominium Variables</h2>
<!-- 	<p><?php //echo $condominium->name;?></p> -->

	<h1>Encapsulation</h1>
	<p>The name of the condominium is <?php echo $condominium->getName();?></p>

	<?php $condominium->setName(300)?>

	<p>The name of the condominium has been changed to <?php echo $condominium->getName();?></p>


	<h2>Player: <?php echo $mage1->getUsername(); ?> of <?php echo $mage1->getGuild(); ?></h2>
	
	<p>
		<?php 
    		echo $mage1->getUsername() . " attacked " . $mage2->getUsername() . "<br>";
    		$mage2->defend($mage1->getLevel() * 10); 
		?>
	</p>
	
	<p>
	<?php
	    $mage1->setGuild('Guild2');
	    echo $mage1->getUsername() . " changed her guild to " . $mage1->getGuild();?>
	</p>
</body>
</html>
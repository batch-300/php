<?php

function divideBy5() {
	$count = 0;

	for ($number = 1; $count < 200; $number++) {
	    if ($number % 5 === 0) {
	        echo $number . " ";
	        $count++;
	    }

	    if ($number === 1000) {
	        break;
	    }
	}

}

$students = array();

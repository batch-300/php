<?php 

//[1] Repetition Control Structures
//Repetition control structures are used to execute code multiple times

//While Loop
//While loop - takes a singel condition
//if the condition evaluates to true, the code inside the block will run

function whileLoop(){

	$count = 5;

	while($count !==0){

		echo $count . '<br/>';
		$count--;

	}


}

// Do-While Loop
// Do-while loop - It works a lot like the while loop, but unlike while loops, do while loops guarantee that the code will be executed at least once.


function doWhileLoop(){

	$count = 20;

	do {
		echo $count. '<br/>';
		$count--;
	}while($count > 0);
}

//For Loop

/*

	for loop is more flexible than while and do-while loops consists of 3 parts
	1. Initial value
	2. Condition
	3. Iteration


*/

function forLoop() {
    for ($count = 0; $count <= 20; $count++) {
        echo $count . '<br/>';
    }
}


// Preferred usage of while vs for loops

// For loops are best used when the number of iteration is known beforehand (iterating over arrays)
//While loops are better when number of iterations is unknown at the time of the loop is defined (depends on user input)


// Continue and break statements

/*
	Continue is a keyword that allows the code to go to the next loop without fnihsing the current code block

	Break is a keyword that ends the execution of the current loop
*/

	function modifiedForLoop() {
	    for ($count = 0; $count <= 20; $count++) {
	        if ($count % 2 === 0) {
	            continue;
	        }
	        echo $count . '<br/>';
	        if ($count > 10) {
	            break;
	        }
	    }
	}

// [2] Array Manipulation
// An Array is a kind of variable that can hold more than one value
// Arrays are delcared using the square brackets '[]' or array() function

	$studentNumbers = array('2020-1923', '2020-1924','2020-1925', '2020-1926','2020-1927'); //before PHP 5.4

	$studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; // Introduced on php 5.4

	// Simple Arrays

	$grades = [98.5, 94.3, 89.2, 90.1];
	$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu', 'HP'];
	$task = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake react'
	];


// Associative Array

	$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

	// Two-Deminesional Array

	$heroes =[
		['ironman', 'thor', 'hulk'],
		['wolverine', 'cyclops', 'jean gray'],
		['batman','superman','wonderwoman']
	];

	// 2-dimensonial associative array

	$ironManPowers =[
		'regular' => ['repulsor blast', 'rocket punch'],
		'signature' => ['unibeam']

	];


// Array Mutations - seek to modify the conents of an array while array iterations aim to evaluate each element in an array


// Array sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);

function searchBrand($brands, $brand) {
    return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the array.";
}


$reversedGradePeriods = array_reverse($gradePeriods);
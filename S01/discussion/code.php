<?php

//It's a rule that we cannot put php codes without a php tag

// We don't have to necessarily add the closing tag.

//[1] Comments
//Comments are part of the code that gets ignored by the language

//Comments are meant to describe the written code.

/*

	Two types of comments:
	-The single-line comment denoted by two slashes
	-The multi-line comment denoted by a slash and asterisk

*/

// [2] Variables

// Variables are used to contain data

// Variable

$name = "John Smith";
$developer = "Peter Parker";
$email = "peterparker@gmail.com";

// Constants

define('PI', 3.1416);
define('HERO', 'Spiderman');

$HERO = "Jose Rizal";

//HERO = "This will not work";

// Data Types

// Strings
$country = "Philippines";
$city = "Quezon City";

$address = $city . ',' .$country;
$address = "$city, $country";

//Integers
$age = 31;
$headcount = 26;
//Floats
$grade = 98.2;
$distanceFromHome = 101.5;

//Boolean
$isGraduating = true;
$isFailed = false;
//Null

$spouse = null;
$middleName = null;

//Arrays
$grades = array (98.7, 92.1, 90.2, 94.6);
$pokemon = ["Bulbausar", "Charmander", "Squirtle"];

//Objects
$gradeObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6,
];

//Miniactivity

$personObj = (object) [
    'firstName' => 'John Doe',
    'isGraduating' => true,
    'age' => 26,
    'address' => (object) [
        'city' => 'Makati City',
        'country' => 'Philippines'
    ]
];

//Assignment Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

//[3] Functions

function getFullName($firstName, $middleInitial, $lastName) {

	return "$lastName, $firstName $middleInitial";

}

//[4] Selection Control Structures

	//If-Elseif-Else statements

function determineTyphoonIntensity($windSpeed) {
    if ($windSpeed < 30) {
        return 'Not a typhoon yet';
    } else if ($windSpeed <= 61) {
        return 'Tropical Depression Detected';
    } else if ($windSpeed >= 62 && $windSpeed <= 88) {
        return 'Tropical Storm Detected';
    } else if ($windSpeed >= 89 && $windSpeed <= 117) {
        return 'Severe Tropical Storm Detected';
    } else {
        return 'Typhoon Detected';
    }
}

// Conditional (ternary) operator

function isUnderAge($age){
	return($age<18) ? true: false;
}

function determineComputerUser($computerNumber) {
    switch ($computerNumber) {
        case 1:
            return 'Naruto';
            break;
        case 2:
            return 'Sasuke';
            break;
        case 3:
            return 'Sakura';
            break;
        default:
            return $computerNumber . ' is out of bounds';
            break;
    }
}

//Try-Catch-Finally

function greeting($str) {
    try {
        if (gettype($str) == "string") {
            echo $str;
        } else {
            throw new Exception("Oops");
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    } finally {
        echo "I did it again!";
    }
}


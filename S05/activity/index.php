<?php
session_start();

if (isset($_SESSION['username'])) {
    echo "Welcome, " . $_SESSION['username'];
?>

<form action="server.php" method="get">
    <input type="hidden" name="logout" value="1">
    <input type="submit" value="Logout">
</form>

<?php
} else {
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
?>

<form action="server.php" method="POST">
    Username: <input type="email" name="username" required><br>
    Password: <input type="password" name="password" required><br>
    <input type="submit" value="Login">
</form>

<?php
}
?>

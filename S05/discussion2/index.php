<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>S:05 Client server communication (basic Task app/to-do list)</title>
</head>
<body>
    <?php session_start(); ?>
    <!-- The session variables are stored on the server and they are not directly viewable in the browser -->
    <!-- This is typically placed at the beginning of the PHP script -->
    <!-- Session data is stored on the server and also allows the server to identify the user's session on multiple requests -->

    <h3>Add Task</h3>

    <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="add" />
        Description: <input type="text" name="description" required>
        <button type="submit">add</button>
    </form>

    <pre><?php var_dump($_SESSION['tasks']) ?></pre>

    <h3>Task List</h3>
    <!-- we want to view our task list -->

    <?php if (isset($_SESSION['tasks'])): ?>
        <?php foreach ($_SESSION['tasks'] as $index => $task): ?>
            <div>
                <form method="POST" action="./server.php" style="display:inline-block;">
                    <input type="hidden" name="action" value="update" />
                    <input type="hidden" name="id" value="<?php echo $index; ?>" />
                    <!-- Value of the index (serve as the id of the task) of the element of the array (SESSION) -->
                    <input type="checkbox" name="isFinished" <?php echo ($task->isFinished) ? 'checked' : null; ?> />
                    <!-- We may know that a task is already finished when there is a check -->
                    <input type="text" name="description" value="<?php echo $task->description; ?>" />
                    <!-- we add the task description value -->
                    <input type="submit" value="Update" />
                </form>
                <form method="POST" action="./server.php" style="display:inline-block;">
                    <input type="hidden" name="action" value="remove" />
                    <input type="hidden" name="id" value="<?php echo $index; ?>" />
                    <input type="submit" value="Delete" />
                </form>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <br/><br/>

    <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="clear" />
        <button type="submit">Clear all tasks</button>
    </form>
</body>
</html>

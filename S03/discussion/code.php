<?php  


// [1] Objects as Variables

$buildingObj = (object) [

	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'

	]

];

$buildingObj2 = (object)[
	"name" => "GMA Network",
	'floors' => 20,
	"Address" => 'EDSA Corner Timog Avenue, Diliman, Quezon City'
];

// [2] Objects from Classes

class Building {

	//this represents a class named Building

	//1. Properties
	//Characteristics of an object
	
	public $name;
	public $floors;
	public $address;

	//2. Constructor Function
	// The class also has a constructor method __construct() that accepts parameters to initalize the properties of the boject

	public function __construct($name, $floors, $address){

		/*
			"$this" keyword refers to the properties and methods within the class scope
			"$this->name" is accessing the "name" property of the current class (Building) and assigning the value of the $name upon instantiation of an object
		*/

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//3. Methods
	// These are functions inside of an objects that can perform a specific action

	public function printName(){
		return "The name of this building is $this->name";
	}
}

//Instantiating Building Class to create a new building object

$building = new Building("Casswyn building",8,"Timod Avenue, Quezon City, Philippines");

//[3] Inhertiance and Polymorphism
// Inhertiance - the derived classes are awllowed to inherit properties and methods from the specified base class
//The "extends" keyword is used to inherit the properties nad methods of a base or parent class

//Parent class => building
// Child class => Condominium

Class Condominium extends Building{

	//the building properties and methods are inherited in this class

	//Polymorphism - Methods inherited by the derived class can be overidden to have a behavior different from the method of the base class

    public function printName() {
        return "The name of this condominium is $this->name";
    }
}

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");

class Pokemon {
    public $type;
    public $level;
    public $trainer;

    public function __construct($type, $level, $trainer) {
        $this->type = $type;
        $this->level = $level;
        $this->trainer = $trainer;
    }

    public function attack() {
        return "The {$this->type} Pokemon attacked!";
    }

    public function faint() {
        return "The {$this->type} Pokemon fainted!";
    }
}
$pokemon1 = new Pokemon("Pikachu", 30, "Ash");
$pokemon2 = new Pokemon("Psyduck", 25, "Misty");
$pokemon3 = new Pokemon("Onyx", 35, "Brock");

class Animal{
	public $name;

	public function __construct($name){
		$this->name = $name;
	}
};

class Dog extends Animal{
	public $breed;

	public function __construct($name,$breed){
		parent::__construct($name);
		$this->breed =$breed;
	}
}

$dog1 = new Dog("whitey", "Chihuahua");

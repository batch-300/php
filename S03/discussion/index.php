<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S03: Classes and Object</title>
</head>
<body>

	<h1>Objects from Variable</h1>
	<p><?php echo $buildingObj->name;?></p>
	<p><?php echo $buildingObj->address ->city;?></p>
	<p><?= print_r($buildingObj)?></p>
	<p><?php echo $buildingObj2->address;?></p>

	<p>Objects from Classes</p>
	<p><?php var_dump($building)?></p>

	<h2>Modify the Instantiated Object</h2>
	<?php $building->name ="GMA Network";?>
	<?php $building->floors ="Twenty one";?>
	<p><?php var_dump($building)?></p>
	<p><?= $building->printName()?></p>

	<h1>Inheritance (Condominium Object)</h1>
	<p><?php var_dump($condominium)?></p>
	<?php $condominium->name;?>
	<?php $building->floors;?>
	<?php $building->name;?>

	<h1>Polymorphism (Overriding the behavior of the printname() method</h1>
		<p><?= $condominium->printname()?></p>

		<h1>Pokemon Information</h1>

		<h2>Pokemon 1</h2>
		<?php var_dump($pokemon1); ?>

		<h2>Pokemon 2</h2>
		<?php var_dump($pokemon2); ?>

		<h2>Pokemon 3</h2>
		<?php var_dump($pokemon3); ?>

		<p><?php var_dump($dog1)?></p>

</body>
</html>
<?php

class Person {
    protected $firstName;
    protected $middleName;
    protected $lastName;

    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName() {
        $fullName = $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName;
        echo "Person: Your full name is $fullName\n";
    }
}

class Developer extends Person {
    public function __construct($firstName, $middleName, $lastName) {
        parent::__construct($firstName, $middleName, $lastName);
    }

    public function printName() {
        $fullName = $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName;
        echo "Developer: Your name is $fullName and you are a developer\n";
    }
}

class Engineer extends Person {
    public function __construct($firstName, $middleName, $lastName) {
        parent::__construct($firstName, $middleName, $lastName);
    }

    public function printName() {
        $fullName = $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName;
        echo "Engineer: You are an engineer named $fullName\n";
    }
}

$person = new Person("Senku", "Ishigami", "");

$developer = new Developer("John", "Finch", "Smith");

$engineer = new Engineer("Harold", "Myers", "Reese");

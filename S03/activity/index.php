<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Person Output</title>
</head>
<body>
    <h1>Person</h1>
    <p>
        <?php
        $person = new Person("Senku", "Ishigami", "");
        $person->printName();
        ?>
    </p>

    <h1>Developer</h1>
    <p>
        <?php
        $developer = new Developer("John", "Finch", "Smith");
        $developer->printName();
        ?>
    </p>

    <h1>Engineer</h1>
    <p>
        <?php
        $engineer = new Engineer("Harold", "Myers", "Reese");
        $engineer->printName();
        ?>
    </p>
</body>
</html>
